db.fruits.aggregate([
  { $match: { onSale: true}},
  { $group: {_id: "fruits on sale", kinds: {$sum:1}}},
 ]);

db.fruits.aggregate([
  { $match: { stock: {$gte : 20}}},
  { $group: { _id: "enough stock", total: { $sum : "$stock" }}}
 ]);

db.fruits.aggregate([
    {
      $match: { onSale: true}},
    {
       $group: {_id: "ave_price", kinds: { $avg: "$price"}}}
  ])

db.fruits.aggregate([
    {
      $group: { _id: "$supplier_id", max_price: {$max: "$price"}}
    }
  ])

  db.fruits.aggregate([
    {
      $group: { _id: "$supplier_id", min_price: {$min: "$price"}}
    }
  ])

